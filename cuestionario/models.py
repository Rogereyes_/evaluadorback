
import cuestionario
from django.db import models

# Create your models here.
class Cuestionario(models.Model):
    name = models.CharField(max_length=200)
    
class Question(models.Model):
    cuestionario = models.ForeignKey(Cuestionario, on_delete=models.CASCADE, related_name='question', null=True, blank=True)
    question_text = models.CharField(max_length=200)   

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='choice')
    choice_text = models.CharField(max_length=200)
    correct = models.BooleanField()

class Evaluado(models.Model):    
    nombre = models.CharField(max_length=200)   
    apellido = models.CharField(max_length=200)
    cedula =  models.CharField(max_length=200)

class Respuesta(models.Model):
    cuestionario = models.ForeignKey(Cuestionario, on_delete=models.CASCADE)
    evaluado =  models.ForeignKey(Evaluado, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    correct = models.BooleanField()