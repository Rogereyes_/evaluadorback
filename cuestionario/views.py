from cuestionario.models import Cuestionario, Evaluado, Question, Choice, Respuesta, cuestionario
from rest_framework import viewsets, generics
from cuestionario.serializer import EvaluadoSerializer, QuiestionSerializer, ChoiceSerializer, CuestionarioSerializer, RespuestaSerializer

class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuiestionSerializer

class ChoiceViewSet(viewsets.ModelViewSet):
    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer

class CuestionarioViewSet(viewsets.ModelViewSet):
    queryset = Cuestionario.objects.all()
    serializer_class = CuestionarioSerializer

class EvaluadosViewSet(viewsets.ModelViewSet):
    queryset = Evaluado.objects.all()
    serializer_class = EvaluadoSerializer

class RespuestaViewSet(viewsets.ModelViewSet):
    queryset = Respuesta.objects.all()
    serializer_class = RespuestaSerializer

class PreguntasCuestionario(generics.ListAPIView):
    serializer_class = QuiestionSerializer
    def get_queryset(self):
        queryset = Question.objects.all()
        cuestionario = self.request.query_params.get('cuestionario')
        if cuestionario is not None:
            queryset = queryset.filter(cuestionario=cuestionario)
        return queryset

class opcionesPregunta(generics.ListAPIView):
    serializer_class = ChoiceSerializer
    def get_queryset(self):
        queryset = Choice.objects.all()
        pregunta = self.request.query_params.get('pregunta')
        if pregunta is not None:
            queryset = queryset.filter(question=pregunta)
        return queryset

class registro(generics.ListAPIView):
    serializer_class = EvaluadoSerializer
    def get_queryset(self):
        queryset = Evaluado.objects.all()
        evaluado = self.request.query_params.get('cedula')
        if evaluado is not None:
            queryset = queryset.filter(cedula=evaluado)
        return queryset