from cuestionario.models import Evaluado, Question, Choice, Cuestionario, Respuesta
from rest_framework import serializers


class QuiestionSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        model = Question
        fields = ['question_text', 'choice', 'cuestionario', 'id']

class ChoiceSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        model = Choice
        fields = ['question', 'choice_text', 'correct', 'id']

class CuestionarioSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        model = Cuestionario
        fields = ['name', 'question', 'id']

class EvaluadoSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        model = Evaluado
        fields = ['nombre', 'apellido', 'cedula', 'id']

class RespuestaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Respuesta
        fields = ['cuestionario', 'evaluado', 'question', 'correct', 'id']