import cuestionario
from django.contrib import admin

# Register your models here.
from .models import Choice, Question, Cuestionario, Evaluado

class QuestionAdmin(admin.ModelAdmin):
    fields = ['question_text', 'cuestionario']

class ChoiceAdmin(admin.ModelAdmin):
    fields = ['question', 'choice_text', 'correct']

class CuestionarioAdmin(admin.ModelAdmin):
    fields = ['name']

class EvaluadoAdmin(admin.ModelAdmin):
    fields = ['nombre', 'apellido', 'cedula']

admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(Cuestionario, CuestionarioAdmin)
admin.site.register(Evaluado, EvaluadoAdmin)
